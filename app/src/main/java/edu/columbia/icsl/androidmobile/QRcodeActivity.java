package edu.columbia.icsl.androidmobile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import edu.columbia.icsl.androidmobile.barcode.BarcodeCaptureActivity;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

public class QRcodeActivity extends AppCompatActivity {
    private static final int BARCODE_READER_REQUEST_CODE = 1;
    HTTP http;
    private String deviceId;
    private TextView mResultTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        mResultTextView = (TextView) findViewById(R.id.result_textview);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Button scanBarcodeButton = (Button) findViewById(R.id.scan_barcode_button);
        scanBarcodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    final Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    Point[] p = barcode.cornerPoints;
                    mResultTextView.setText(barcode.displayValue);
                    final String[] separated = barcode.displayValue.split(" ");
                    if (separated.length < 2 || !separated[0].equals("ICSL")) {
                        return;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    // 2. Chain together various setter methods to set the dialog characteristics
                    builder.setMessage("This device's energy consumption will be attributed to your account.").setTitle("Is this your device?");

                    //Value == Rewards/body| Key == "messageID"

                    //Button One : Yes
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            http = new HTTP(getApplicationContext());

                            final String messageID = separated[1];
                            http.newDevice(deviceId, messageID, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response.equals("0")) {
                                        Log.d("NEWDEVICE", "GOT NEW DEVICE");
                                    }

                                    //Data is in response, need to make it into a JSONObject and pull
                                    // suggestions array. Update listview data from here
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.i("HTTP", error.toString());
                                }
                            });
                        }
                    });
                    //Button Two : No
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    // 3. Get the AlertDialog from create()
                    final AlertDialog dialog = builder.create();
                    /*dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                                 @Override
                                                 public void onShow(DialogInterface arg0) {
                                                     dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0x00FF00);
                                                 }
                                             });*/
                    dialog.show();
                    Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    nbutton.setBackgroundColor(Color.DKGRAY);
                    Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    pbutton.setBackgroundColor(Color.DKGRAY);
                } else mResultTextView.setText(R.string.no_barcode_captured);
            } else Log.e(LOG_TAG, String.format(getString(R.string.barcode_error_format),
                    CommonStatusCodes.getStatusCodeString(resultCode)));
        } else super.onActivityResult(requestCode, resultCode, data);
    }
}
