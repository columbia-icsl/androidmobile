package edu.columbia.icsl.androidmobile;

import android.app.IntentService;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RecService extends IntentService {

    public static String androidId;

    public HTTP http;


    public RecService() {
        super("RecService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        http =  new HTTP(getBaseContext());
        androidId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        RecommenderFragment.getRecommendations(http, androidId);
    }
}
