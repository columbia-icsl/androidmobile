package edu.columbia.icsl.androidmobile;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DeviceWatcher extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        BluetoothDevice device =  intent.getParcelableExtra("EXTRA_DEVICE_DISCOVERED_DEVICE");
        Log.d("BleScanService", device.toString());
        // do anything with this information

    }
}