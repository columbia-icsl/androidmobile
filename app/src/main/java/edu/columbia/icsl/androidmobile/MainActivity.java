package edu.columbia.icsl.androidmobile;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    public static String androidId;
    public BluetoothAdapter mBluetoothAdapter;
    Context context;
    private static final long SCAN_PERIOD = 5000;
    public static RewardsFragment rewards;
    private ProfileFragment profile;
    private TabLayout tabLayout;
    Typeface futura;
    private RecommenderFragment recommender;
    public static HomeFragment home;
    private EnergyFragment energy;
    private String deviceId;
    private HTTP http;
    int UPDATE_TIME_GRAPHS_UI = 20000;
    int UPDATE_TIME_BEACONS = 10000; //10 mins
    Timer bluetoothTimer;
    String location = "";
    MyReceiver myReceiver;
    GoogleApiClient googleClient;


    public void scheduleAlarm() {
        Intent intent = new Intent(getApplicationContext(), RecReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, RecReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis();
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, 30*1000, pIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlarmReceiver alarm = new AlarmReceiver();
        alarm.setAlarm(this);
        setContentView(R.layout.tab_layout);
        scheduleAlarm();

        futura = Typeface.createFromAsset(this.getAssets(), "Futura-Light.ttf");

        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tabanim_toolbar);
        setSupportActionBar(toolbar);
        ViewPager viewPager = (ViewPager) findViewById(R.id.tabanim_viewpager);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = (TabLayout) findViewById(R.id.tabanim_tabs);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont();
        viewPager.setCurrentItem(1);

        context = getApplicationContext();
        http = new HTTP(context);

        this.getPermissions();
        updateFragmentData();

        myReceiver = new MyReceiver()  {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                String data = bundle.getString(BleService.DATA);
                try {
                    JSONObject json = new JSONObject(data);
                    location = json.getString("location");
                    JSONArray suggestion = json.getJSONArray("suggestions");
                    Log.i("Sugg", suggestion.toString());
                    rewards.updateSuggestions(suggestion);
                    rewards.setRewardsPie(json.getInt("balance"), json.getInt("tempBalance"));
                    home.setRewardsPie(json.getInt("balance"), json.getInt("tempBalance"));
                }
                catch (JSONException e) {
                    Log.i("Error", "error on parsing ble reciever");
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BleService.DATA);
        registerReceiver(myReceiver, intentFilter);

        //connectToWatch();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if(init == false) {
            UPDATE_TIME_BEACONS /= FACTOR_BEACONS;
            bluetoothTimer.purge();
            setUpBluetoothTimer();
        }
        else {
            init = false;
        }*/
        Log.i("Time", UPDATE_TIME_BEACONS + "");
    }

    @Override
    protected void onStop()
    {
        try {
            unregisterReceiver(myReceiver);
        }
        catch (IllegalArgumentException e){

        }
        super.onStop();
    }

    private void setUpBluetooth() {
        BluetoothManager bluetoothManager = (BluetoothManager)
                this.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                int REQUEST_ENABLE_BT = 1;
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else {
                Log.i("BLE", "bluetooth on");
            }
        } else {
            Log.i("RECENTDEBUG", "Bluetooth Adapter is null!");
        }
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(futura);
                }
            }
        }
    }

    private void getPermissions() {
        androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
            }
        } else {
            this.setUpBluetooth();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    this.setUpBluetooth();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        boolean tutor = getIntent().getBooleanExtra("startTutorial", false);
        adapter.addFrag(home = new HomeFragment(), "Home");
        adapter.addFrag(rewards = new RewardsFragment(), "Rewards");
        adapter.addFrag(recommender = new RecommenderFragment(), "Recs");
        home.startTutor(tutor);
        adapter.addFrag(profile = new ProfileFragment(), "Profile");
        viewPager.setAdapter(adapter);
    }

    private void updateFragmentData() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getDataHTTP();
            }
        }, 5000, UPDATE_TIME_GRAPHS_UI);
    }
    private void getDataHTTP() {
        http.getData(deviceId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Long value = json.getLong("value");
                    Long hvac = json.getLong("HVAC");
                    Long light = json.getLong("Light");
                    Long electrical = json.getLong("Electrical");
                    home.setDataHTTP(value, hvac, light, electrical, location);
                    //new SendToDataLayerThread("/energy_path", json.toString()).start();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("HTTP", error.toString());
            }
        });
    }
    @Override
    public void onBackPressed() {
    }
    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            String data = arg1.getStringExtra(BleService.DATA);

            Intent i = new Intent().putExtra(BleService.DATA, data);
            context.sendBroadcast(i);
        }

    }

    private void connectToWatch() {
        if(null == googleClient) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            if(!googleClient.isConnected())
                googleClient.connect();
        }
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d("myTag", cause+"");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("myTag", connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d("myTag", "connected");
    }

    class SendToDataLayerThread extends Thread {
        String path;
        String message;

        // Constructor to send a message to the data layer
        SendToDataLayerThread(String p, String msg) {
            path = p;
            message = msg;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await(10, TimeUnit.SECONDS);
            for (Node node : nodes.getNodes()) {
                SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await(20, TimeUnit.SECONDS);
                if (result.getStatus().isSuccess()) {
                    Log.v("myTag", "Message: {" + message + "} sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.v("myTag", "ERROR: failed to send Message");
                }
            }
        }
    }
}

