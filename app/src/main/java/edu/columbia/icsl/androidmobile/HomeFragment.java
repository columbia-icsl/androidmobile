package edu.columbia.icsl.androidmobile;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

import static edu.columbia.icsl.androidmobile.SharedFunctions.getEnergyHTML;


/**
 * Created by jordanvega on 1/29/17.
 */

public class HomeFragment extends Fragment {
    int color;
    private LineChart mChart;
    private PieChart rewardsPie;
    private PieChart energyPie;
    Typeface futura;
    String location;
    ShowcaseView showcaseView;
    int count;
    boolean ifStart;
    LineDataSet set1;
    LineDataSet set2;
    LineDataSet set3;


    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.home, container, false);

        futura = Typeface.createFromAsset(getContext().getAssets(), "Futura-Light.ttf");
        TextView rewardsLabel = (TextView) view.findViewById(R.id.rewardsLabel);
        TextView energyLabel = (TextView) view.findViewById(R.id.energyLabel);
        TextView chartLabel = (TextView) view.findViewById(R.id.chartLabel);


        rewardsLabel.setTypeface(futura);
        energyLabel.setTypeface(futura);
        chartLabel.setTypeface(futura);

        TextPaint titlePaint = new TextPaint();
        titlePaint.setAntiAlias(true);
        titlePaint.setTextSize(95f);
        titlePaint.setColor(getResources().getColor(R.color.appWhite));
        titlePaint.setTypeface(futura);


        lineChart(view);
        pieCharts(view);
        count = 0;
        if(ifStart) {
            showcaseView = new ShowcaseView.Builder(getActivity())
                    .setTarget(Target.NONE)
                    .setOnClickListener(onClickListener)
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setContentTextPaint(titlePaint)
                    .setContentTitlePaint(titlePaint)
                    .build();

            showcaseView.setContentTitle("Welcome! Here is a quick tutorial");
            showcaseView.setButtonText("Start!");
        }
        CardView energyCard = (CardView) view.findViewById(R.id.energy_cardview);
        energyCard.setPreventCornerOverlap(true);
        CardView rewardsCard = (CardView) view.findViewById(R.id.rewards_cardview);
        rewardsCard.setPreventCornerOverlap(true);

        energyCard.setCardElevation(2);
        energyCard.setPadding(10, 10, 10, 10);

        rewardsCard.setCardElevation(2);
        rewardsCard.setPadding(10, 10, 10, 10);

        final CardView grpahCard = (CardView) view.findViewById(R.id.graph_card);
        grpahCard.setCardElevation(2);
        grpahCard.setPadding(10, 10, 10, 10);
        return view;
    }

    public void startTutor(boolean ifStart) {
        this.ifStart = ifStart;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (count) {
                case 0:
                    showcaseView.setButtonText("Next");
                    showcaseView.setContentTitle("The bottom graph shows your current energy" +
                            " footprint.");
                    showcaseView.setShowcase(new ViewTarget(mChart), true);
                    break;
                case 1:
                    showcaseView.setContentTitle("The energy ring shows your energy breakdown.");
                    showcaseView.setShowcase(new ViewTarget(energyPie), true);
                    break;

                case 2:
                    showcaseView.setContentTitle("The reward ring shows your progress " +
                            "towards an Amazon gift card!");
                    showcaseView.setButtonText("Close");
                    showcaseView.setShowcase(new ViewTarget(rewardsPie), true);
                    break;

                case 3:
                    showcaseView.hide();
                    break;
            }
            count++;

        }
    };

    public void pieCharts(View view) {
        energyPie = (PieChart) view.findViewById(R.id.energyProgressBar);
        rewardsPie = (PieChart) view.findViewById(R.id.rewardsProgressBar);

        energyPie.getDescription().setEnabled(false);
        rewardsPie.getDescription().setEnabled(false);

        rewardsPie.setDragDecelerationFrictionCoef(0.95f);

        rewardsPie.setCenterTextTypeface(futura);
        rewardsPie.setCenterText("Loading...");
        rewardsPie.setCenterTextSize(20.f);
        rewardsPie.setCenterTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        rewardsPie.setHoleColor(Color.TRANSPARENT);

        energyPie.setCenterTextTypeface(futura);
        energyPie.setCenterText(SharedFunctions.getEnergyHTML("0"));
        energyPie.setCenterTextSize(20.f);
        energyPie.setCenterTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        energyPie.setHoleColor(Color.TRANSPARENT);

        energyPie.setDrawHoleEnabled(true);
        rewardsPie.setDrawHoleEnabled(true);

        rewardsPie.setTransparentCircleColor(Color.TRANSPARENT);
        rewardsPie.setTransparentCircleAlpha(0);
        energyPie.setTransparentCircleColor(Color.TRANSPARENT);
        energyPie.setTransparentCircleAlpha(0);

        rewardsPie.setHoleRadius(SharedFunctions.pieWidth);
        energyPie.setHoleRadius(SharedFunctions.pieWidth);

        rewardsPie.setDrawCenterText(true);
        energyPie.setDrawCenterText(true);

        rewardsPie.setRotationAngle(0);

        float mult = 100;

        ArrayList<PieEntry> entries = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5), 10*i));
        }
        setDataForPies(entries, entries);

        rewardsPie.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        energyPie.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        rewardsPie.setTouchEnabled(false);
        energyPie.setTouchEnabled(false);

    }

    public void lineChart(View view){
        mChart = (LineChart) view.findViewById(R.id.chart1);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        //mChart.setTouchEnabled(false);

        mChart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(false);

        // set an alternative background color
        mChart.setViewPortOffsets(20, 15, 30, 0);

        // add data
        setData(100, 30);
        mChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setGranularity(1f); // one hour


        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        mChart.setDrawGridBackground(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);
        leftAxis.setTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        leftAxis.setDrawLabels(true);
        leftAxis.setDrawTopYLabelEntry(true);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.format("%d W",(long)value);

            }
        });
        mChart.getXAxis().setDrawGridLines(false);
    }

    private void setDataForPies( ArrayList<PieEntry> r, ArrayList<PieEntry> nrg) {
        ArrayList<PieEntry> rewards = new ArrayList<>();
        rewards.add(new PieEntry(1));
        PieDataSet dataSet = new PieDataSet(rewards, "");
        PieDataSet nrgDataSet = new PieDataSet(rewards, "");


        dataSet.setColors(new int[] { R.color.appGrey}, getContext());

        PieData data = new PieData(dataSet);
        data.setValueTextSize(0f);


        rewardsPie.setData(data);
        rewardsPie.getLegend().setEnabled(false);

        rewardsPie.highlightValues(null);

        rewardsPie.invalidate();


        nrgDataSet.setColors(new int[] { R.color.appGrey}, getContext());
        PieData nrgdata = new PieData(nrgDataSet);
        nrgdata.setValueTextSize(0f);
        energyPie.setData(nrgdata);
        energyPie.getLegend().setEnabled(false);

        energyPie.highlightValues(null);

        energyPie.invalidate();
    }

    private void setData(int count, float range) {

        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(0,0));
        values.add(new Entry(1,0));
        values.add(new Entry(2,0));
        values.add(new Entry(3,0));
        values.add(new Entry(4,0));
        values.add(new Entry(5,0));
        values.add(new Entry(6,0));
        values.add(new Entry(7,0));
        values.add(new Entry(8,0));
        values.add(new Entry(9,0));

        ArrayList<Entry> values1 = new ArrayList<>();
        values1.add(new Entry(0,0));
        values1.add(new Entry(1,0));
        values1.add(new Entry(2,0));
        values1.add(new Entry(3,0));
        values1.add(new Entry(4,0));
        values1.add(new Entry(5,0));
        values1.add(new Entry(6,0));
        values1.add(new Entry(7,0));
        values1.add(new Entry(8,0));
        values1.add(new Entry(9,0));

        ArrayList<Entry> values2 = new ArrayList<>();
        values2.add(new Entry(0,0));
        values2.add(new Entry(1,0));
        values2.add(new Entry(2,0));
        values2.add(new Entry(3,0));
        values2.add(new Entry(4,0));
        values2.add(new Entry(5,0));
        values2.add(new Entry(6,0));
        values2.add(new Entry(7,0));
        values2.add(new Entry(8,0));
        values2.add(new Entry(9,0));

        // create a dataset and give it a type
        set1 = new LineDataSet(values, "HVAC");
        set1.setColors(new int[] {R.color.HVAC}, getContext());
        set1.setLineWidth(1.5f);
        set1.setDrawCircles(true);
        set1.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set1.setDrawValues(false);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.WHITE);
        set1.setHighLightColor(Color.WHITE);
        set1.setDrawFilled(true);
        set1.setFillColor(getResources().getColor(R.color.HVAC));


        set2 = new LineDataSet(values1, "LIGHT");
        set2.setColors(new int[] {R.color.LIGHT}, getContext());
        set2.setLineWidth(1.5f);
        set2.setDrawCircles(true);
        set2.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set2.setDrawValues(false);
        set2.setFillAlpha(65);
        set2.setFillColor(Color.WHITE);
        set2.setHighLightColor(Color.WHITE);
        set2.setDrawFilled(true);
        set2.setFillColor(getResources().getColor(R.color.LIGHT));

        set3 = new LineDataSet(values2, "ELECTRIC");
        set3.setColors(new int[] {R.color.ELECTRIC}, getContext());
        set3.setLineWidth(1.5f);
        set3.setDrawCircles(true);
        set3.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set3.setDrawValues(false);
        set3.setFillAlpha(65);
        set3.setFillColor(Color.WHITE);
        set3.setHighLightColor(Color.WHITE);
        set3.setDrawFilled(true);
        set3.setFillColor(getResources().getColor(R.color.ELECTRIC));

        LineData data = new LineData();
        data.addDataSet(set1);
        data.addDataSet(set2);
        data.addDataSet(set3);

        // set data
        mChart.setData(data);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setTouchEnabled(true);
        CustomMarkerView mv = new CustomMarkerView(getContext(), R.layout.marker);
        mChart.setMarker(mv);
    }

    public void setDataHTTP(Long total, Long hvac, Long light, Long electrical, String location) {
        this.location = location;
        ArrayList<PieEntry> entries = new ArrayList<>();
        if(total > 0) {
            entries.add(new PieEntry(hvac));
            entries.add(new PieEntry(light));
            entries.add(new PieEntry(electrical));
            addEnergyPieData(entries, false);
        }
        else {
            entries.add(new PieEntry(1));
            addEnergyPieData(entries, true);
        }

        LineData data = mChart.getData();
        if (data != null) {
            // at least one entry exists
            LineDataSet set = (LineDataSet) data.getDataSetByIndex(0);
            // check and remove oldest entry
            if (set.getEntryCount() == 10) {
                set.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            LineDataSet set1 = (LineDataSet) data.getDataSetByIndex(1);
            // check and remove oldest entry
            if (set1.getEntryCount() == 10) {
                set1.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set1.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            LineDataSet set2 = (LineDataSet) data.getDataSetByIndex(2);
            // check and remove oldest entry
            if (set2.getEntryCount() == 10) {
                set2.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set2.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            Entry e = new Entry(set.getEntryCount(), hvac);
            Entry e1 = new Entry(set.getEntryCount(), light+hvac);
            Entry e2 = new Entry(set.getEntryCount(), light+hvac+electrical);
            e.setData(location);
            e1.setData(location);
            e2.setData(location);
            data.addEntry(e, 0);
            data.addEntry(e1, 1);
            data.addEntry(e2, 2);

        }

        mChart.invalidate();
        mChart.notifyDataSetChanged();
        energyPie.setCenterText(getEnergyHTML(total.toString()));
        energyPie.notifyDataSetChanged();
        energyPie.invalidate();
    }

    private void addEnergyPieData( ArrayList <PieEntry> entries, boolean ifZero) {
        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setDrawValues(false);
        if(!ifZero)
            pieDataSet.setColors(new int[] {R.color.HVAC, R.color.LIGHT, R.color.ELECTRIC}, getContext());
        else
            pieDataSet.setColors(new int[]{R.color.appGrey}, getContext());

        PieData pieData = new PieData(pieDataSet);
        energyPie.setData(pieData);
    }

    public void setRewardsPie(int balance, int tempbal){
        ArrayList<PieEntry> entries = new ArrayList<>();
        PieData pieData;
        if(balance == 0 && tempbal == 0) {
            entries.add(new PieEntry(1));

            PieDataSet pieDataSet = new PieDataSet(entries, "");
            pieDataSet.setDrawValues(false);
            pieDataSet.setColors(new int[]{R.color.appGrey}, getContext());
            rewardsPie.setCenterText(String.valueOf(0));

            pieData = new PieData(pieDataSet);
        }
        else {
            int maxBal = 25;
            if ((balance % maxBal) > ((balance + tempbal) % maxBal)) {
                entries.add(new PieEntry(balance % maxBal));
                entries.add(new PieEntry(maxBal - (balance % maxBal)));
                entries.add(new PieEntry(0));
            } else {
                entries.add(new PieEntry(balance % maxBal));
                entries.add(new PieEntry(tempbal % maxBal));
                entries.add(new PieEntry(maxBal - ((balance+tempbal) % maxBal)));
            }


            PieDataSet pieDataSet = new PieDataSet(entries, "");
            pieDataSet.setDrawValues(false);
            pieDataSet.setColors(new int[]{R.color.appBlueGrey, R.color.appGreen, R.color.appGrey}, getContext());

            pieData = new PieData(pieDataSet);
            rewardsPie.setCenterText(String.valueOf(balance));
        }
        rewardsPie.setData(pieData);
        rewardsPie.notifyDataSetChanged();
        rewardsPie.invalidate();
    }

}