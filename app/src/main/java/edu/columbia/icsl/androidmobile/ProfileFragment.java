package edu.columbia.icsl.androidmobile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import static edu.columbia.icsl.androidmobile.R.layout.profile;
/**
 * Created by peterwei on 3/19/17.
 */


public class ProfileFragment extends Fragment {
    private String deviceId;
    public ProfileFragment() {}

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceId = Settings.Secure.getString(this.getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        View view = inflater.inflate(profile, container, false);
        Bitmap bm = BitmapFactory.decodeResource(getResources(),
                R.drawable.profile_image);
        ImageView profileImage = (ImageView) view.findViewById(R.id.profileImage);
        profileImage.setImageBitmap(getCircleBitmap(bm));
        final Button logout = (Button) view.findViewById(R.id.logout);
        final Button viewTutorial = (Button) view.findViewById(R.id.viewTutorial);
        final Button QRButton = (Button) view.findViewById(R.id.QRscanner);
        final Button aboutUs = (Button) view.findViewById(R.id.aboutUs);
        TextView fullName = (TextView) view.findViewById(R.id.fullName);
        TextView labName = (TextView) view.findViewById(R.id.labName);
        TextView energySaved = (TextView) view.findViewById(R.id.energySaved);
        TextView suggestionsTaken = (TextView) view.findViewById(R.id.suggestionsTaken);
        TextView rewardsClaimed = (TextView) view.findViewById(R.id.rewardsClaimed);

        fullName.setText(staticInformationClass.getUsername());
        labName.setText(staticInformationClass.getLabname());
        energySaved.setText(String.valueOf(staticInformationClass.getEnergySaved()) + " W");
        suggestionsTaken.setText(String.valueOf(staticInformationClass.getSuggestionsTaken()));
        rewardsClaimed.setText(String.valueOf(staticInformationClass.getRewardsClaimed()));
        viewTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewTutorial();
            }
        });
        QRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QRscanner();
            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutUs();
            }
        });
        logout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                logout();
            }
        });
        return view;
    }

    public void viewTutorial() {
        Log.i("Tutorial", "About to go to the tutorial");
        Intent i = new Intent(getContext(), PagerActivity.class);
        startActivity(i);
    }

    public void aboutUs() {
        Intent i = new Intent(getContext(), aboutUsActivity.class);
        startActivity(i);
    }

    public void QRscanner() {
        Intent i = new Intent(getContext(), QRcodeActivity.class);
        startActivity(i);
    }

    public void logout() {
        HTTP http = new HTTP(getContext());
        Log.i("Test", "in");
        http.logout(deviceId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("0")) {
                    Intent i = new Intent(getContext(), CreateAccountActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getContext(), "Signout Failed, " +
                                    "please try again",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("HTTP", error.getMessage().toString());
            }
        });
    }


}
