package edu.columbia.icsl.androidmobile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomGrid extends BaseAdapter{
    private Context mContext;
    private final ArrayList<String> web;
    private final ArrayList<String> reward;
    private final ArrayList<String> imageArray;
    private final ArrayList<String> message;
    private final ArrayList<String> body;
    private final int[] Imageid;
    private final int[] colors;
    private final ArrayList mData;

    public CustomGrid(HashMap<String, HashMap<String, String>> map, Context c, ArrayList<String> web,
                      ArrayList<String> message, ArrayList<String> reward, ArrayList<String> body,
                      int[] Imageid,  ArrayList<String> imageArray, int[] colors) {
        mContext = c;
        this.Imageid = Imageid;
        this.web = web;
        this.reward = reward;
        this.colors = colors;
        this.imageArray = imageArray;
        this.message = message;
        this.body = body;
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.size();
    }

    @Override
    public View getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("Get view on position: ", Integer.toString(position));
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.gridsingle, null);
            TextView textView = (TextView) grid.findViewById(R.id.grid_text);
            ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
            MyLinearLayout ML = (MyLinearLayout) grid.findViewById(R.id.gridsingle);
            TextView textView2 = (TextView) grid.findViewById(R.id.grid_reward);

            ML.setBackgroundColor(colors[position]);
            textView.setText(web.get(position));
            String x = imageArray.get(position);
            Log.i("IMAGE", x);
            if (x.equals("move")) {
                imageView.setImageResource(Imageid[0]);
            }
            if (x.equals("shift")) {
                imageView.setImageResource(Imageid[1]);
            }
            if (x.equals("plug")) {
                imageView.setImageResource(Imageid[2]);
            }
            if (x.equals("shade")) {
                imageView.setImageResource(Imageid[3]);
            }

            String RS = "Reward: ";
            textView2.setText(RS + reward.get(position));
            //Button theButton = (Button)grid.findViewById(R.id.invisButton);
            //theButton.setVisibility(View.VISIBLE);
            //theButton.setBackgroundColor(Color.TRANSPARENT);

            /*theButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                }
            });*/
        } else {
            grid = (View) convertView;
        }

        return grid;
    }


}