package edu.columbia.icsl.androidmobile;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import static edu.columbia.icsl.androidmobile.R.layout.sign_up;

//////////////////////////////////////
import  java.security.NoSuchAlgorithmException;
//////////////////////////////////////


public class CreateAccountActivity extends AppCompatActivity {

    EditText email;
    EditText username;
    EditText password;
    String deviceId;
    HTTP http;
    Typeface futura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        futura = Typeface.createFromAsset(
                this.getApplication().getApplicationContext().getAssets(), "Futura-Light.ttf");

        http = new HTTP(this.getApplicationContext());

        checkUser();
    }

    public void signUp(View v) {
        String user = username.getText().toString().replace(" ", "");
        String pass = password.getText().toString().replace(" ", "");

        /////////////////////////////////////////////////////
        try {
            pass = SHAhash.sha1(pass);
        } catch (NoSuchAlgorithmException e) {
            pass = pass;
        }
        /////////////////////////////////////////////////////

        String eml = email.getText().toString().replace(" ", "");
        if (user.length() != 0) {
            http.newUser(deviceId, user, eml, pass, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equals("0")) {
                        Intent i = new Intent(getApplicationContext(), PagerActivity.class);
                        //i.putExtra("startTutorial", true);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "Registration Failed, " +
                                        "please try again",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("HTTP", error.getMessage().toString());
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(),
                    "Username must not be blank!", Toast.LENGTH_SHORT).show();
        }
    }

    public void toLogin(View v) {
        setContentView(R.layout.login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        Button signup = (Button) findViewById(R.id.signup_button);
        signup.setTypeface(futura);
        email.setTypeface(futura);
        password.setTypeface(futura);
    }

    public void signIn(View v) {
        String pass = password.getText().toString().replace(" ", "");

        ////////////////////////////////////////////////
        try {
            pass = SHAhash.sha1(pass);
        } catch (NoSuchAlgorithmException e) {
            pass = pass;
        }
        ////////////////////////////////////////////////


        String eml = email.getText().toString().replace(" ", "");
        if (eml.length() != 0) {
            http.returningUser(deviceId, eml, pass, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equals("0")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("startTutorial", false);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "Email/Password incorrect, " +
                                        "please try again",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("HTTP", error.getMessage().toString());
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(),
                    "Username must not be blank!", Toast.LENGTH_SHORT).show();
        }
    }

private void checkUser(){
    http.newIfUserExists(deviceId, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            if (response != null) {
                try {
                    JSONObject json = new JSONObject(response);
                    String status = json.getString("status");
                    staticInformationClass.setUsername(json.getString("username"));
                    staticInformationClass.setLabname(json.getString("labname"));
                    staticInformationClass.setEnergySaved(json.getInt("energySaved"));
                    staticInformationClass.setRewardsClaimed(json.getInt("rewardsClaimed"));
                    staticInformationClass.setSuggestionsTaken(json.getInt("suggestionsTaken"));
                    if (status.equals("0")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                    } else if (status.equals("1")) {
                        setContentView(R.layout.login);
                        email = (EditText) findViewById(R.id.email);
                        password = (EditText) findViewById(R.id.password);
                        Button signup = (Button) findViewById(R.id.signup_button);
                        signup.setTypeface(futura);
                        email.setTypeface(futura);
                        password.setTypeface(futura);
                    } else if (status.equals("404")) {
                        setContentView(sign_up);
                        username = (EditText) findViewById(R.id.username);
                        email = (EditText) findViewById(R.id.email);
                        password = (EditText) findViewById(R.id.password);
                        Button signup = (Button) findViewById(R.id.signup_button);
                        signup.setTypeface(futura);
                        username.setTypeface(futura);
                        email.setTypeface(futura);
                        password.setTypeface(futura);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.i("HTTP", error.toString());
        }
    });
}
}