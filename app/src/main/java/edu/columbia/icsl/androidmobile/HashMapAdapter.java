package edu.columbia.icsl.androidmobile;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HashMapAdapter extends BaseAdapter {
    private final ArrayList mData;
    Typeface futura;


    public HashMapAdapter(HashMap<String, HashMap<String, String>> map, Context context) {
        mData = new ArrayList();
        mData.addAll(map.entrySet());
        futura = Typeface.createFromAsset(context.getAssets(), "Futura-Light.ttf");

    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, HashMap<String, String>> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        } else {
            result = convertView;
        }

        Map.Entry<String, HashMap<String, String>> item = getItem(position);
        String title = item.getValue().get(SharedFunctions.TITLE_KEY);
        String reward = item.getValue().get(SharedFunctions.REWARD_KEY);

        // TODO replace findViewById by ViewHolder
        TextView titleText = (TextView) result.findViewById(R.id.title);
        titleText.setText(title);
        titleText.setTypeface(futura);
        titleText.setTextColor(Color.BLACK);

        TextView view = (TextView) result.findViewById(R.id.rewards);
        view.setText(reward);
        view.setTypeface(futura);
        view.setTextColor(Color.BLACK);

        return result;
    }

    public void addAll(HashMap<String, HashMap<String, String>>map) {
        mData.clear();
        mData.addAll(map.entrySet());
        this.notifyDataSetChanged();
    }

    public void remove(int position){
        mData.remove(position);
    }
}