package edu.columbia.icsl.androidmobile;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static edu.columbia.icsl.androidmobile.R.layout.rewards;

/**
 * Created by jordanvega on 1/29/17.
 */

public class RewardsFragment extends Fragment {
    int color;
    ListView listView;
    HashMapAdapter adapter;
    Typeface futura;
    private PieChart rewardsPie;
    HTTP http;
    private String deviceId;


    public RewardsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(rewards, container, false);

        deviceId = Settings.Secure.getString(this.getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        listView = (ListView) view.findViewById(R.id.listView);
        futura = Typeface.createFromAsset(getContext().getAssets(), "Futura-Light.ttf");
        TextView label = (TextView) view.findViewById(R.id.rewardsLabel);
        label.setTypeface(futura);

        TextView giftLabel = (TextView) view.findViewById(R.id.giftLabel);
        giftLabel.setTypeface(futura);

        TextView suggLabel = (TextView) view.findViewById(R.id.suggestions_label);
        suggLabel.setTypeface(futura);

        HashMap<String, HashMap<String, String>> map = new HashMap<>();
        pieCharts(view);


        adapter = new HashMapAdapter(map, getContext());

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                // int itemPosition     = position;

                // ListView Clicked item value
                Map.Entry<String, HashMap<String, String>> itemValue
                        = (Map.Entry<String, HashMap<String, String>>) listView.getItemAtPosition(position);

                String messageID = itemValue.getKey();

                HashMap<String, String> innerMap = itemValue.getValue();
                // Show Alert
                //Toast.makeText(getContext(),
                //       "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                //        .show();

                callOnClick(itemValue, position);

            }

        });
        return view;
    }

    public void callOnClick(final Map.Entry<String, HashMap<String, String>> map, final int position) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(map.getValue().get(SharedFunctions.BODY_KEY)).setTitle("Accept Suggestion?");

        //Value == Rewards/body| Key == "messageID"

        //Button One : Yes
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), "Suggestion redeemed!", Toast.LENGTH_LONG).show();
                http = new HTTP(getContext());
                final String messageID = map.getKey();
                String reward = map.getValue().get(SharedFunctions.REWARD_KEY);

                http.acceptSuggestion(deviceId, messageID, reward, "true", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("success")) {
                            adapter.remove(position);
                            adapter.notifyDataSetChanged();
                        }


                        //Data is in response, need to make it into a JSONObject and pull
                        // suggestions array. Update listview data from here
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HTTP", error.toString());
                    }
                });
            }
        });
        //Button Two : No
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), "You didn't accept the suggestion :(", Toast.LENGTH_LONG).show();
                dialog.cancel();
            }
        });
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void updateSuggestions(JSONArray json) throws JSONException {
        //adapter.clear();
        HashMap<String, HashMap<String, String>> suggest = new HashMap<>();

        for (int i = 0; i < json.length(); i++) {
            JSONObject obj = json.getJSONObject(i);
            HashMap<String, String> inner = new HashMap<>();

            inner.put(SharedFunctions.BODY_KEY, obj.get("body").toString());
            inner.put(SharedFunctions.REWARD_KEY, obj.get("reward").toString());
            inner.put(SharedFunctions.TITLE_KEY, obj.get("title").toString());

            suggest.put((String) obj.get("messageID"), inner);

        }
        adapter.addAll(suggest);
    }

    public void pieCharts(View view) {
        rewardsPie = (PieChart) view.findViewById(R.id.rewardsProgressBar);
        rewardsPie.setTouchEnabled(false);

        rewardsPie.getDescription().setEnabled(false);

        rewardsPie.setCenterTextTypeface(futura);
        rewardsPie.setCenterText("Loading...");
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        rewardsPie.setCenterTextSize(width / 60);
        rewardsPie.setCenterTextColor(Color.BLACK);
        rewardsPie.setHoleColor(Color.TRANSPARENT);

        rewardsPie.setDrawHoleEnabled(true);

        rewardsPie.setTransparentCircleColor(Color.TRANSPARENT);
        rewardsPie.setTransparentCircleAlpha(0);


        rewardsPie.setHoleRadius(SharedFunctions.pieWidth);

        rewardsPie.setDrawCenterText(true);

        rewardsPie.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        setDataRewardsPie(3, 20);

    }

    private void setDataRewardsPie(int count, float range) {

        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        entries.add(new PieEntry(1));

        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setDrawValues(false);
        pieDataSet.setColors(new int[]{R.color.appGrey}, getContext());

        PieData pieData = new PieData(pieDataSet);

        rewardsPie.setData(pieData);
        rewardsPie.getLegend().setEnabled(false);

        // undo all highlights
        rewardsPie.highlightValues(null);

        rewardsPie.invalidate();
    }

    public void parseMap(String str) {

    }


    public void setRewardsPie(int balance, int tempbal) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        PieData pieData;
        if (balance == 0 && tempbal == 0) {
            entries.add(new PieEntry(1));

            PieDataSet pieDataSet = new PieDataSet(entries, "");
            pieDataSet.setDrawValues(false);
            pieDataSet.setColors(new int[]{R.color.appGrey}, getContext());
            rewardsPie.setCenterText(String.valueOf(0));


            pieData = new PieData(pieDataSet);
        } else {
            int maxBal = 250;
            if ((balance % maxBal) > ((balance + tempbal) % maxBal)) {
                entries.add(new PieEntry(balance % maxBal));
                entries.add(new PieEntry(maxBal - (balance % maxBal)));
                entries.add(new PieEntry(0));
            } else {
                entries.add(new PieEntry(balance % maxBal));
                entries.add(new PieEntry(tempbal % maxBal));
                entries.add(new PieEntry(maxBal - ((balance+tempbal) % maxBal)));
            }

            PieDataSet pieDataSet = new PieDataSet(entries, "");
            pieDataSet.setDrawValues(false);
            pieDataSet.setColors(new int[]{R.color.appBlueGrey, R.color.appGreen, R.color.appGrey}, getContext());

            pieData = new PieData(pieDataSet);
            rewardsPie.setCenterText(String.valueOf(balance));
        }
        rewardsPie.setData(pieData);
        rewardsPie.notifyDataSetChanged();
        rewardsPie.invalidate();
    }

}
