package edu.columbia.icsl.androidmobile;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;


public class EnergyFragment extends Fragment{
    PieChart energyPie;
    private LineChart mChart;
    Typeface futura;
    TextView hvacText;
    TextView lightingText;
    TextView plugmeterText;
    LineDataSet set1;
    LineDataSet set2;
    LineDataSet set3;


    public EnergyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.energy, container, false);

        futura = Typeface.createFromAsset(getContext().getAssets(), "Futura-Light.ttf");
        hvacText = (TextView) view.findViewById(R.id.hvac_text);
        lightingText = (TextView) view.findViewById(R.id.light_text);
        plugmeterText = (TextView) view.findViewById(R.id.electrical_text);
        TextView energyLabel = (TextView) view.findViewById(R.id.energyLabel);
        energyLabel.setTypeface(futura);
        hvacText.setTypeface(futura);
        lightingText.setTypeface(futura);
        plugmeterText.setTypeface(futura);


        energyPie = (PieChart) view.findViewById(R.id.energyProgressBar);
        lineChart(view);
        pieCharts(view);

        return view;

    }
    public void lineChart(View view){
        mChart = (LineChart) view.findViewById(R.id.chart);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(false);

        mChart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(false);

        // set an alternative background color
        mChart.setViewPortOffsets(20, 15, 30, 0);

        // add data
        setData();
        mChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        xAxis.setTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setGranularity(1f); // one hour


        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        mChart.setDrawGridBackground(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);
        leftAxis.setTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        leftAxis.setDrawLabels(true);
        leftAxis.setDrawTopYLabelEntry(true);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.format("%dW",(long)value);

            }
        });

        mChart.getXAxis().setDrawGridLines(false);
    }

    private void setData() {

        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(0,0));
        values.add(new Entry(1,0));
        values.add(new Entry(2,0));
        values.add(new Entry(3,0));
        values.add(new Entry(4,0));
        values.add(new Entry(5,0));
        values.add(new Entry(6,0));
        values.add(new Entry(7,0));
        values.add(new Entry(8,0));
        values.add(new Entry(9,0));

        ArrayList<Entry> values1 = new ArrayList<>();
        values1.add(new Entry(0,0));
        values1.add(new Entry(1,0));
        values1.add(new Entry(2,0));
        values1.add(new Entry(3,0));
        values1.add(new Entry(4,0));
        values1.add(new Entry(5,0));
        values1.add(new Entry(6,0));
        values1.add(new Entry(7,0));
        values1.add(new Entry(8,0));
        values1.add(new Entry(9,0));

        ArrayList<Entry> values2 = new ArrayList<>();
        values2.add(new Entry(0,0));
        values2.add(new Entry(1,0));
        values2.add(new Entry(2,0));
        values2.add(new Entry(3,0));
        values2.add(new Entry(4,0));
        values2.add(new Entry(5,0));
        values2.add(new Entry(6,0));
        values2.add(new Entry(7,0));
        values2.add(new Entry(8,0));
        values2.add(new Entry(9,0));

        set1 = new LineDataSet(values, "HVAC");
        set1.setColors(new int[] {R.color.HVAC}, getContext());
        set1.setLineWidth(1.5f);
        set1.setDrawCircles(true);
        set1.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set1.setDrawValues(false);
        set1.setFillAlpha(65);
        set1.setFillColor(Color.WHITE);
        set1.setHighLightColor(Color.WHITE);
        set1.setDrawFilled(true);
        set1.setFillColor(getResources().getColor(R.color.HVAC));


        set2 = new LineDataSet(values1, "LIGHT");
        set2.setColors(new int[] {R.color.LIGHT}, getContext());
        set2.setLineWidth(1.5f);
        set2.setDrawCircles(true);
        set2.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set2.setDrawValues(false);
        set2.setFillAlpha(65);
        set2.setFillColor(Color.WHITE);
        set2.setHighLightColor(Color.WHITE);
        set2.setDrawFilled(true);
        set2.setFillColor(getResources().getColor(R.color.LIGHT));

        set3 = new LineDataSet(values2, "ELECTRIC");
        set3.setColors(new int[] {R.color.ELECTRIC}, getContext());
        set3.setLineWidth(1.5f);
        set3.setDrawCircles(true);
        set3.setCircleColor(SharedFunctions.colorTemplateTextColor(getResources()));
        set3.setDrawValues(false);
        set3.setFillAlpha(65);
        set3.setFillColor(Color.WHITE);
        set3.setHighLightColor(Color.WHITE);
        set3.setDrawFilled(true);
        set3.setFillColor(getResources().getColor(R.color.ELECTRIC));

        LineData data = new LineData();
        data.addDataSet(set1);
        data.addDataSet(set2);
        data.addDataSet(set3);

        // set data
        mChart.setData(data);
    }

    public void setDataHTTP(long total, Long hvac, Long light, Long electrical) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        if(total > 0) {
            entries.add(new PieEntry(hvac));
            entries.add(new PieEntry(light));
            entries.add(new PieEntry(electrical));
            addEnergyPieData(entries, false);
        }
        else {
            entries.add(new PieEntry(1));
            addEnergyPieData(entries, true);
        }


        hvacText.setText(SharedFunctions.energyLabelText("HVAC", hvac));
        lightingText.setText(SharedFunctions.energyLabelText("LIGHT", light));
        plugmeterText.setText(SharedFunctions.energyLabelText("ELECTRIC", electrical));

        LineData data = mChart.getData();
        if (data != null) {
            // at least one entry exists
            LineDataSet set = (LineDataSet) data.getDataSetByIndex(0);
            // check and remove oldest entry
            if (set.getEntryCount() == 10) {
                set.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            LineDataSet set1 = (LineDataSet) data.getDataSetByIndex(1);
            // check and remove oldest entry
            if (set1.getEntryCount() == 10) {
                set1.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set1.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            LineDataSet set2 = (LineDataSet) data.getDataSetByIndex(2);
            // check and remove oldest entry
            if (set2.getEntryCount() == 10) {
                set2.removeEntry(0); // remove oldest

                // change Indexes - move to beginning by 1
                for (Entry entry : set2.getValues()) {
                    entry.setX(entry.getX() - 1);
                }
            }
            data.addEntry(new Entry(set.getEntryCount(), hvac), 0);
            data.addEntry(new Entry(set1.getEntryCount(), light+hvac), 1);
            data.addEntry(new Entry(set2.getEntryCount(), electrical+light+hvac), 2);

        }

        mChart.invalidate();
        mChart.notifyDataSetChanged();
        energyPie.setCenterText(SharedFunctions.getEnergyHTML(String.valueOf((int)total)));
        energyPie.notifyDataSetChanged();
        energyPie.invalidate();
    }

    public void pieCharts(View view) {
        energyPie = (PieChart) view.findViewById(R.id.energyProgressBar);

        energyPie.getDescription().setEnabled(false);

        energyPie.setCenterTextTypeface(futura);
        energyPie.setCenterText(SharedFunctions.getEnergyHTML("0"));
        energyPie.setCenterTextSize(20.f);
        energyPie.setCenterTextColor(SharedFunctions.colorTemplateTextColor(getResources()));
        energyPie.setHoleColor(Color.TRANSPARENT);

        energyPie.setDrawHoleEnabled(true);

        energyPie.setTransparentCircleColor(Color.TRANSPARENT);
        energyPie.setTransparentCircleAlpha(0);

        energyPie.setHoleRadius(SharedFunctions.pieWidth);

        energyPie.setDrawCenterText(true);

        float mult = 100;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
        for (int i = 0; i < 2; i++) {
            entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5), 10*i));
        }
        setDataForPies(entries);

        energyPie.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        energyPie.setTouchEnabled(false);

    }

    private void setDataForPies( ArrayList<PieEntry> nrg) {

        PieDataSet nrgDataSet = new PieDataSet(nrg, "");

        nrgDataSet.setColors(new int[] { R.color.appRed, R.color.appGrey}, getContext());
        PieData nrgdata = new PieData(nrgDataSet);
        nrgdata.setValueTextSize(0f);
        energyPie.setData(nrgdata);
        energyPie.getLegend().setEnabled(false);

        energyPie.highlightValues(null);

        energyPie.invalidate();
    }

    private void addEnergyPieData( ArrayList <PieEntry> entries, boolean ifZero) {
        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setDrawValues(false);
        if(!ifZero)
            pieDataSet.setColors(new int[] {R.color.HVAC, R.color.LIGHT, R.color.ELECTRIC}, getContext());
        else
            pieDataSet.setColors(new int[]{R.color.appGrey}, getContext());

        PieData pieData = new PieData(pieDataSet);
        energyPie.setData(pieData);
    }

}
