package edu.columbia.icsl.androidmobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RecReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, RecService.class);
        context.startService(i);
    }



}
