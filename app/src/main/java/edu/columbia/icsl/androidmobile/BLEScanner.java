package edu.columbia.icsl.androidmobile;

import android.util.Log;

import java.util.ArrayList;




    class BeaconLookup {
        private static final String lookup_table_raw = "" +
                "Beacon Number\tiOS Service ID\tDevice Label\tMAC Address\n" +
                "1\tf900016e04e64984\t1A73828449E6046E\t\n" +
                "2\tf90001690d1fac13\t\t\n" +
                "3\tf90001799c2ce28b\t6034838BE22C9C79\tgone\n" +
                "4\tf9000169a6d0e462\t99902662E4D0A669\tD9:FB:DB:3C:51:65\n" +
                "5\tf900015acb890ba4\t74548EA40B89CB5A\tDF:DC:B0:91:1D:90\n" +
                "6\tf90001925ceed04f\t9361B64FD0EE5C92\tED:04:E6:33:1C:56\n" +
                "7\tf90001445622a8b5\tF2ED54B5A8225644\tE7:C2:8E:5A:8D:EF\n" +
                "8\tf900018a16945342\t1A7AAE425394168A\tD3:E9:AE:D8:4C:8D\n" +
                "9\t\tED35FC3FB2965B53\tCC:DD:C4:31:80:BC\n" +
                "10\tf90001ced6688472\tD05E28728468D6CE\tF5:1F:15:CD:D4:A9\n" +
                "11\tf9000151ca814933\t27D3A1334981CA51\tDF:DA:7D:7C:BF:F7\n" +
                "12\tf9000154265338ec\tCB2ECFEC38532654\tCB:F4:76:BA:D3:BC\n" +
                "13\tf900018ff0d461ce\tD26737CE61D4F08F\tFC:FC:95:5B:E5:21\n" +
                "14\tf900019d32d333dc\t5EE437DC33D3329D\tE5:A4:0E:27:0C:04\n" +
                "15\tf90001b5e1f2a607\tF01F3307A6F2E1B5\tE0:E4:6E:CC:8F:A6\n" +
                "16\tf90001cdf639767e\t0E08C37E7639F6CD\tD3:28:7D:B0:3D:E4\n" +
                "17\tf90001c99a18ea37\tA56B2D37EA189AC9\tF2:FD:E3:05:61:07\n" +
                "18\tf90001bd1866d42a\t7064522AD46618BD\tDA:15:7C:C2:BF:56\n" +
                "19\tf90001dd870b0d1e\t5FC84A1E0D0B87DD\tFC:53:F8:EB:51:E7\n" +
                "20\tf900010218cb2e63\t16E6D4632ECB1802\tFD:35:3D:86:45:8A\n" +
                "21\tf900012720b39dde\tB6E408DE9DB32027\tEB:A2:27:2B:35:14\n" +
                "22\tf900017e9d0fb84e\t5C9F504EB80F9D7E\tE3:A2:BD:B9:61:26\n" +
                "23\tf90001fbee27a1cf\tD51CC0CFA127EEFB\tE0:A4:2C:AE:51:20\n" +
                "24\tf90001cbea3a1904\t59137104193AEACB\tCA:5F:22:32:93:A6\n" +
                "25\tf9000143e013e15e\t6318AD5EE113E043\tDC:C9:FD:C4:C0:07\n" +
                "26\tf900018d9a41d879\t09168979D8419A8D\tCD:E6:D7:6F:17:32\n" +
                "27\tf90001665d0025a8\t20344FA825005D66\tDE:CA:B0:D6:97:BE\n" +
                "28\tf9000177c9214b4a\t5FE5284A4B21C977\tD6:17:D8:9F:13:A2\n" +
                "29\tf900015c274b097b\t5334A67B094B275C\tE8:0B:D8:AB:8D:0F\n" +
                "30\tf90001ec526f628f\t162F798F626F52EC\tFF:A6:7D:AB:35:04\n" +
                "31\tf90001b6d0ec2da2\t4915E8A22DECD0B6\tD3:FF:15:3D:18:C0\n" +
                "32\tf900017b053062d4\t37E8ECD46230057B\tC5:1C:67:B4:F9:06\n" +
                "33\tf90001e11c126311\t6745021163121CE1\tEA:73:06:B9:B3:B3\n" +
                "34\tf90001435cbdac75\tEA9DC575ACBD5C43\tE6:2A:54:8D:00:C5\n" +
                "35\tf90001173b78520c\tBB38B80C52783B17\tDB:B8:A7:B0:E1:D5\n" +
                "36\tf900015bc6df9635\tD4CCBA3596DFC65B\tD7:47:F1:01:1D:2B\n" +
                "37\tf90001be8b0e254e\tFF82AD4E250E8BBE\tF9:09:A7:DC:8E:D9\n" +
                "38\tf90001e541ec397e\t85D0997E39EC41E5\tFD:CF:D7:8E:A8:6F\n" +
                "39\tf9000151a9bc0719\t99B6AD1907BCA951\tC7:3A:2F:E8:0B:0B\n" +
                "40\tf900012edca22762\t48B8076227A2DC2E\tE2:55:9A:E2:5F:91\n" +
                "41\tf900013b3f9eb6ef\t0A6C73EFB69E3F3B\tFD:4E:27:8C:14:4B";
        public static int N = -1;
        private static java.util.HashMap<String, Integer> lookupTable = null;

        public BeaconLookup() {
            if (lookupTable == null) {
                String[] items = lookup_table_raw.split("\n");
                N = items.length - 1;
                Log.i("BeaconLookup", "Table parsed: total#=" + N);

                lookupTable = new java.util.HashMap<String, Integer>();
                for (int i = 0; i <= N; i++) {
                    String line = items[i];
                    String arr[] = line.split("\t");
                    if (arr.length < 4) {
                        Log.w("BeaconLookup", "Illegitimate line parsed:" + line);
                        continue;
                    }
                    String mac = arr[3];
                    if (mac.length() != 12 + 5) {
                        Log.w("BeaconLookup", "Illegitimate line parsed:" + line);
                        continue;
                    }
                    mac = mac.toLowerCase();
                    lookupTable.put(mac, i);
                }
            }
        }

        public int lookup_macAddress_string(String mac) {
            mac = mac.toLowerCase();
            if (lookupTable.containsKey(mac))
                return lookupTable.get(mac);
            return 0;
        }
    }


class VectorResultConstructor {
    static final int defaultRSSI = -100;
    static final int maxSlot = 1000;
    BeaconLookup lookup;
    int[] Avg_RSSI_Values;
    ArrayList<Integer> values_samples[];
    Integer new_samples[];
    boolean beaconsFound = false;
    Integer unknownLocationCounter = 0;

    @SuppressWarnings("unchecked")
    public VectorResultConstructor() {
        lookup = new BeaconLookup();

        values_samples = new ArrayList[lookup.N + 1];
        for (int i = 1; i <= lookup.N; i++) {
            values_samples[i] = new ArrayList<Integer>();
        }
        new_samples = new Integer[lookup.N + 1];
        for (int i = 1; i < lookup.N; i++) {
            new_samples[i] = defaultRSSI;
        }
    }

    public void reinitializeArray() {
        for (int i = 1; i <= lookup.N; i++) {
            new_samples[i] = defaultRSSI;
        }
        beaconsFound = false;
    }

    public void clearHistory() {
        for (int i = 1; i <= lookup.N; i++) {
            values_samples[i] = new ArrayList<Integer>();
        }
    }

    public boolean InsertSample(String mac, int rssi) {
        int idx = lookup.lookup_macAddress_string(mac);
        Log.i("BeaconNum", String.valueOf(idx));
        if (idx == 0 || idx > lookup.N)
            return false;

        synchronized (this) {
            beaconsFound = true;
            Log.i("FOUNDBEACONS", String.valueOf(beaconsFound));
            values_samples[idx].add(rssi);
            new_samples[idx] = rssi;
        }

        return true;
    }

    public boolean foundBeacons() {
        return beaconsFound;
    }

    public boolean unknownLocation() {
        Log.i("UNKNOWN LOCATION", unknownLocationCounter.toString());
        return (unknownLocationCounter > 5);
    }

    public String toString() {
        StringBuilder ret = new StringBuilder(BleService.androidId);
        Avg_RSSI_Values = new int[lookup.N + 1];

        synchronized (this) {
            Log.i("FOUNDBEACONS", String.valueOf(beaconsFound));
            if (beaconsFound == false) {
                unknownLocationCounter++;
                if (unknownLocationCounter > 5) {
                    clearHistory();
                }
                for (int i = 1; i <= lookup.N; i++) {
                    Avg_RSSI_Values[i] = defaultRSSI;
                }
            }
            else {
                unknownLocationCounter = 0;
                for (int i = 1; i <= lookup.N; i++) {
                    if (values_samples[i].size() == 0)
                        Avg_RSSI_Values[i] = defaultRSSI;
                    else {
                        int sum = 0;
                        for (int rssi : values_samples[i])
                            sum += rssi;
                        Avg_RSSI_Values[i] = (sum + new_samples[i]) / (values_samples[i].size()+1);
                    }
                }
            }
        }

        for (int i = 1; i <= lookup.N; i++) {
            ret.append(",");
            ret.append(Avg_RSSI_Values[i]);
        }
        return ret.toString();
    }

    public boolean receivedAny() {
        synchronized (this) {
            for (int i = 1; i <= lookup.N; i++)
                if (values_samples[i].size() != 0)
                    return true;
        }
        return false;
    }
}



