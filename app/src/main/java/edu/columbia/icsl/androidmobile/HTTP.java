package edu.columbia.icsl.androidmobile;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class HTTP {
    Context context;
    static String url = "http://icsl.ee.columbia.edu:8000/api/";
    static String POST_BEACON_DATA = "Beacons/";
    static String POST_USERS = "userManagement/";
    static String POST_DEVICE = "addDevice/";
    static String POST_CHECK_USERS = "checkUser/";
    static String POST_RETURN_USERS = "returnUser/";
    static String POST_NEW_USERS = "newUser/";
    static String POST_LOGIN = "login/";
    static String POST_LOGOUT = "logout/";
    static String APP_SUPPORT = "appSupport/?id=";
    static String sug_url = "suggestionDecisions/";
    static String uploadNotificationsToken = "userManagement/addPush/";
    static String POST_RECS = "getRecs/?id=";
    private static RequestQueue queue;



    public HTTP(Context context) {
        //ID variables?
        this.context = context;
        queue = Volley.newRequestQueue(context);
    }

    public void ifUserExists(final String deviceID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener ) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_CHECK_USERS, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return String.format("%s", deviceID).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void newIfUserExists(final String deviceID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_RETURN_USERS, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return String.format("%s", deviceID).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void postBeaconData(final String beaconData, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_BEACON_DATA, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return beaconData.getBytes();
            }
        };
        Log.i("RETRY", "Setting retry policy");
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void newUser(final String deviceID, final String username,final String email, final String password, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_NEW_USERS, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {

                return String.format("%s,%s,%s,%s", deviceID, username, email, password).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    public void returningUser(final String deviceID,final String email, final String password, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_LOGIN, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return String.format("%s,%s,%s", deviceID , email, password).getBytes();
            }
        };
        Log.i(email, password);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void getData(final String deviceID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url+APP_SUPPORT+deviceID, responseHandler, errorListener );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void getRecs(final String deviceID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url+POST_RECS+deviceID, responseHandler, errorListener );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void acceptSuggestion(final String deviceID, final String messageID, final String reward, final String decision, Response.Listener<String> responseHandler, Response.ErrorListener errorListener){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+sug_url, responseHandler, errorListener){
            @Override
            public byte[] getBody() throws AuthFailureError{
                return String.format("%s,%s,%s,%s", deviceID, messageID, reward, decision).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void newDevice(final String deviceID, final String messageID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_DEVICE, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError{
                return String.format("%s,%s", deviceID, messageID).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public void logout(final String deviceID, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+POST_USERS+POST_LOGOUT, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return String.format("%s", deviceID ).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
    public void uploadToken(final String deviceID, final String token, Response.Listener<String> responseHandler, Response.ErrorListener errorListener) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url+uploadNotificationsToken, responseHandler, errorListener ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return String.format("%s,%s", deviceID, token).getBytes();
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
        Log.d("Token", "Token sent!");
    }
}
