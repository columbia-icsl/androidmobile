package edu.columbia.icsl.androidmobile;

import android.util.Log;

/**
 * Created by peterwei on 3/19/17.
 */

public class staticInformationClass {
    private static String username = "";
    private static String labname = "";
    private static Integer energySaved = 0;
    private static Integer rewardsClaimed = 0;
    private static Integer suggestionsTaken = 0;
    public static String getUsername() {return username;}
    public static String getLabname() {return labname;}
    public static Integer getEnergySaved() {return energySaved;}
    public static Integer getRewardsClaimed() {return rewardsClaimed;}
    public static Integer getSuggestionsTaken() {return suggestionsTaken;}
    public static void setUsername(String S) {
        Log.i("StaticCalled", S);
        staticInformationClass.username = S;}
    public static void setLabname(String S) {staticInformationClass.labname = S;}
    public static void setEnergySaved(Integer I) {staticInformationClass.energySaved = I;}
    public static void setRewardsClaimed(Integer I) {staticInformationClass.rewardsClaimed = I;}
    public static void setSuggestionsTaken(Integer I) {staticInformationClass.suggestionsTaken = I;}
}
