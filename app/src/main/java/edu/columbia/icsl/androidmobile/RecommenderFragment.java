package edu.columbia.icsl.androidmobile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;


public class RecommenderFragment extends Fragment {
    public static ArrayList<String> web = new ArrayList<String>();
    public static ArrayList<String> messageArray = new ArrayList<String>();
    public static ArrayList<String> rewardArray = new ArrayList<String>();
    public static ArrayList<String> bodyArray = new ArrayList<String>();
    public static ArrayList<String> ImageArray = new ArrayList<>();
    public static int tempBal;
    public static int bal;
    GridView gridview;
    public static CustomGrid adapter;
    public static int[] imageId = {
            R.drawable.running,
            R.drawable.clock,
            R.drawable.plug,
            R.drawable.shade,
            R.drawable.running,

    };
    public static int[] colors = {
            Color.parseColor("#4ecdc4"),
            Color.parseColor("#b9e3c6"),
            Color.parseColor("#8693ab"),
            Color.parseColor("#81d2c7"),
            Color.parseColor("#8fbfe0"),
            Color.parseColor("#444444")
    };
    public String deviceID;
    public RecommenderFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i("On create function ", "called");
        super.onCreateView(inflater, container, savedInstanceState);
        deviceID = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        View view = inflater.inflate(R.layout.recommender, container, false);

        HashMap<String, HashMap<String, String>> map = new HashMap<>();
        adapter = new CustomGrid(map, view.getContext(), web, messageArray, rewardArray, bodyArray, imageId, ImageArray, colors);
        gridview = (GridView) view.findViewById(R.id.recGridView);
        gridview.setNumColumns(2);
        gridview.setAdapter(adapter);



        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Log.i("ONCLICK Position", Integer.toString(position));
                callOnClick(position);
                Log.i("ONCLICK", "call on click finished");
                //gridview.invalidateViews();
                //Log.i("ONCLICK", "invalidate views finished");
                //gridview.setAdapter(adapter);
                //Log.i("ONCLICK", "set adaptor finished");
            }
        });

        final SwipeRefreshLayout mySwipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i("GridView", "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        myUpdateOperation();
                        mySwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
        return view;
    }

    public void myUpdateOperation() {
        String deviceID = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        HTTP http = new HTTP(this.getContext());

        getRecommendations(http, deviceID);
        adapter.notifyDataSetChanged();
        Log.i("GridView", "Refreshed!");

        ////////////////////////////////////////////////
        gridview.invalidateViews();
        gridview.setAdapter(adapter);
        ////////////////////////////////////////////////

    }

    public void callOnClick(final int position) {
        // 1. Instantiate an AlertDialog.Builder with its constructor

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String deviceId = deviceID;
        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(bodyArray.get(position)).setTitle("Accept Suggestion?");

        //Value == Rewards/body| Key == "messageID"
        //Button One : Yes
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), "Suggestion redeemed!", Toast.LENGTH_LONG).show();
                HTTP http = new HTTP(getContext());
                final String messageID = messageArray.get(position);

                String reward = rewardArray.get(position);

                final int r = Integer.parseInt(reward);
                http.acceptSuggestion(deviceId, messageID, reward, "true", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("success")) {
                            web.remove(position);
                            bodyArray.remove(position);
                            messageArray.remove(position);
                            rewardArray.remove(position);
                            ImageArray.remove(position);
                            adapter.notifyDataSetChanged();
                            Log.i("Removed Position", Integer.toString(position));

                            ///////////////////////////////////////////////
                            gridview.invalidateViews();
                            Log.i("ONCLICK", "invalidate views finished");
                            gridview.setAdapter(adapter);
                            Log.i("ONCLICK", "set adaptor finished");
                            ///////////////////////////////////////////////


                            MainActivity.rewards.setRewardsPie(bal, tempBal + r);
                            MainActivity.home.setRewardsPie(bal, tempBal + r);
                        }
                        //Data is in response, need to make it into a JSONObject and pull
                        // suggestions array. Update listview data from here
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HTTP", error.toString());
                    }
                });
            }
        });
        //Button Two : No
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), "Suggestion redeemed!", Toast.LENGTH_LONG).show();
                HTTP http = new HTTP(getContext());
                final String messageID = messageArray.get(position);

                String reward = rewardArray.get(position);
                final int r = Integer.parseInt(reward);
                http.acceptSuggestion(deviceId, messageID, reward, "false", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("success")) {
                            web.remove(position);
                            bodyArray.remove(position);
                            messageArray.remove(position);
                            rewardArray.remove(position);
                            ImageArray.remove(position);
                            adapter.notifyDataSetChanged();
                            Log.i("Removed Position", Integer.toString(position));

                            /////////////////////////////////////////////
                            gridview.invalidateViews();
                            Log.i("ONCLICK", "invalidate views finished");
                            gridview.setAdapter(adapter);
                            Log.i("ONCLICK", "set adaptor finished");
                            /////////////////////////////////////////////


                            MainActivity.rewards.setRewardsPie(bal, tempBal + r);
                            MainActivity.home.setRewardsPie(bal, tempBal + r);
                        }
                        //Data is in response, need to make it into a JSONObject and pull
                        // suggestions array. Update listview data from here
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("HTTP", error.toString());
                    }
                });
                //Toast.makeText(getContext(), "You didn't accept the suggestion :(", Toast.LENGTH_LONG).show();
                dialog.cancel();
            }
        });
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
        Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        Button b1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button b2 = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
        if (b != null) {
            b.setTextColor(getResources().getColor(R.color.appBlueGrey, null));
        }
        if (b1 != null) {
            b1.setTextColor(getResources().getColor(R.color.appRed, null));
        }
        if (b2 != null) {
            b2.setTextColor(getResources().getColor(R.color.appBlack, null));
        }

    }


    public static void getRecommendations(HTTP http, String deviceID) {
        web.clear();
        bodyArray.clear();
        messageArray.clear();
        rewardArray.clear();
        ImageArray.clear();
        http.getRecs(deviceID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("HTTP", response);
                try {
                    RecommenderFragment.web.clear();
                    RecommenderFragment.bodyArray.clear();
                    RecommenderFragment.messageArray.clear();
                    RecommenderFragment.rewardArray.clear();
                    RecommenderFragment.ImageArray.clear();
                    JSONObject json = new JSONObject(response);
                    JSONArray ja = (JSONArray) json.get("suggestions");
                    bal = (int) json.get("balance");
                    tempBal = (int) json.get("tempBalance");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = ja.getJSONObject(i);
                        String t = (String) jo.get("title");
                        String b = (String) jo.get("body");
                        String m = (String) jo.get("messageID");
                        String r = (String) String.valueOf(jo.get("reward"));
                        String ty = (String) jo.get("type");
                        RecommenderFragment.web.add(t);
                        RecommenderFragment.bodyArray.add(b);
                        RecommenderFragment.messageArray.add(m);
                        RecommenderFragment.rewardArray.add(r);
                        RecommenderFragment.ImageArray.add(ty);

                    }
                    if (RecommenderFragment.adapter != null) {
                        RecommenderFragment.adapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("HTTP", error.toString());
            }
        });
    }
}