package edu.columbia.icsl.androidmobile;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BleService extends IntentService implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static VectorResultConstructor resultConstructor;
    boolean startScan = true;
    public BluetoothAdapter mBluetoothAdapter;
    private static final long SCAN_PERIOD = 5000;
    public static String androidId;
    public HTTP http;
    final static String DATA = "DATA";

    GoogleApiClient googleClient;
    String deviceId;


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public BleService() {
        super("SchedulingService");
        Log.d("constructor", "starting...");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        http =  new HTTP(getBaseContext());
        androidId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        resultConstructor = new VectorResultConstructor();
        BluetoothManager bluetoothManager = (BluetoothManager)
                this.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        connectToWatch();
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        try {
            scanForBeacons();
        }
        catch (JSONException e) {
            Log.i("BLE", e.toString());
        }
        Log.i("BLE SERVICE", "SCAN");
        //AlarmReceiver.completeWakefulIntent(intent);
    }

    public void scanForBeacons() throws JSONException {
        final Handler mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                Log.i("Handler", message.toString());
            }
        };


        if (startScan) {
            // Stops scanning after a pre-defined scan period.

            startScan = false;
            final BluetoothManager bluetoothManager = (BluetoothManager)
                    this.getSystemService(Context.BLUETOOTH_SERVICE);
            resultConstructor.reinitializeArray();
            if (mBluetoothAdapter.getBluetoothLeScanner() == null) {

                mBluetoothAdapter = bluetoothManager.getAdapter();
            } else {
                mBluetoothAdapter.getBluetoothLeScanner().startScan(mLeScanCallback);
            }
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mBluetoothAdapter != null && mLeScanCallback != null
                            && !mBluetoothAdapter.isDiscovering()) {
                        try {
                            mBluetoothAdapter.getBluetoothLeScanner().stopScan(mLeScanCallback);
                        } catch (NullPointerException e) {
                            Log.i("BLE NULL", e.toString());
                        }
                        String result = resultConstructor.toString();
                        Log.i("Results", result);
                        postBeaconData(result, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject json = new JSONObject(response);
                                            Intent intent = new Intent();
                                            intent.setAction(DATA);

                                            intent.putExtra(DATA, json.toString());
                                            new SendToDataLayerThread("/message_path", json.toString()).start();

                                            sendBroadcast(intent);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //Data is in response, need to make it into a JSONObject and pull
                                        // suggestions array. Update listview data from here

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.i("HTTP", error.toString());
                                    }
                                }
                        );
                        http.getData(deviceId, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                new SendToDataLayerThread("/message_path", response).start();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("HTTP", error.toString());
                            }
                        });
                    } else {
                        Log.i("HTTP SCAN", "Caught null adapter and callback");
                    }
                }
            }, SCAN_PERIOD);
        } else {

        }
    }

    // Device scan callback.
    private ScanCallback mLeScanCallback =
            new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    if (resultConstructor != null) {
                        Log.i("MAC", result.getDevice().getAddress());
                        Log.i("MAC", String.valueOf(result.getRssi()));

                        resultConstructor.InsertSample(result.getDevice().getAddress(), result.getRssi());
                    } else {
                        Log.w("BLEScanner", "NULL result constructor");
                    }
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {

                }

                @Override
                public void onScanFailed(int errorCode) {
                    Log.i("Error", errorCode+"");
                }
            };


    private void postBeaconData(final String result, Response.Listener responseListener,
                                Response.ErrorListener errorListener) {
        http.postBeaconData(result, responseListener, errorListener);
    }

    private void connectToWatch() {
        if(null == googleClient) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            if(!googleClient.isConnected())
                googleClient.connect();
        }
    }

    // Placeholders for required connection callbacks
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d("myTag", cause+"");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Log.d("myTag", connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d("myTag", "connected");
    }

    class SendToDataLayerThread extends Thread {
        String path;
        String message;

        // Constructor to send a message to the data layer
        SendToDataLayerThread(String p, String msg) {
            path = p;
            message = msg;
        }

    public void run() {
        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await(10, TimeUnit.SECONDS);

        for (final Node node : nodes.getNodes()) {
            SendMessageResult result = Wearable.MessageApi.sendMessage(googleClient, node.getId(), path, message.getBytes()).await(20, TimeUnit.SECONDS);
            if (result.getStatus().isSuccess()) {
                Log.v("myTag", "Message: {" + message + "} sent to: " + node.getDisplayName());
            }
            else {
                // Log an error
                Log.v("myTag", "ERROR: failed to send Message");
            }
        }
    }
}


}
