package edu.columbia.icsl.androidmobile;

import android.text.Html;
import android.text.Spanned;

/**
 * Created by jordanvega on 2/19/17.
 */

public class SharedFunctions {

    public static Spanned getEnergyHTML(String value, String label) {
        return Html.fromHtml(label+value+"<sup><small>w</small></sup>");
    }

    public static Spanned energyLabelText(String label, double val) {
        return Html.fromHtml(String.format("%s: %.1f", label, val) + "<sup><small>w</small></sup>");
    }

    public static String BODY_KEY = "Body";
    public static String REWARD_KEY = "Reward";
    public static String TITLE_KEY = "Title";
    public static float pieWidth = 97f;

}
