package edu.columbia.icsl.androidmobile;

/**
 * Created by jordanvega on 5/2/17.
 */

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Listens to DataItems and Messages from the local node.
 */
public class DataLayerListenerService extends WearableListenerService {
    final static String DATA = "DATA";
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v("myTag", "service started!");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.v("myTag", "Message received ");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.v("myTag", "Message received ");
        if (messageEvent.getPath().equals("/message_path")) {
            final String message = new String(messageEvent.getData());
            //Log.v("myTag", "Message path received on watch is: " + messageEvent.getPath());
            Log.v("myTag", "Message received on watch is: " + message);
            Intent intent = new Intent();
            intent.setAction(DATA);

            intent.putExtra(DATA, message);

            sendBroadcast(intent);
        }
        else {
            super.onMessageReceived(messageEvent);
            Log.v("myTag", "No message");
        }
    }
}