package edu.columbia.icsl.androidmobile;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * The main activity with a view pager, containing three pages:<p/>
 * <ul>
 * <li>
 * Page 1: shows a list of DataItems received from the phone application
 * </li>
 * <li>
 * Page 2: shows the photo that is sent from the phone application
 * </li>
 * <li>
 * Page 3: includes two buttons to show the connected phone and watch devices
 * </li>
 * </ul>
 */
public class MainActivity extends Activity  {

    private static final String TAG = "MainActivity";
    MyReceiver myReceiver;
    TextView location;
    TextView energy;
    String total;
    String hvac;
    String light;
    String electrical;
    int count = 1;
    PieChart energyPie;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(getApplicationContext(),DataLayerListenerService.class));
        location = (TextView)findViewById(R.id.location);
        energy = (TextView)findViewById(R.id.energy);
        energyPie = (PieChart) findViewById(R.id.energyProgressBar);
        energyPie.setHoleColor(Color.TRANSPARENT);
        energyPie.setTransparentCircleColor(Color.TRANSPARENT);
        energyPie.setTransparentCircleAlpha(0);
        energyPie.setHoleRadius(85);
        Log.d("pie",energyPie.getHoleRadius()+"");
        energyPie.getLegend().setEnabled(false);
        energyPie.highlightValues(null);
        energyPie.invalidate();
        energyPie.setTouchEnabled(false);
        energyPie.setCenterTextColor(getResources().getColor(R.color.black));
        Description des = energyPie.getDescription();
        des.setEnabled(false);

        ArrayList<PieEntry> entries = new ArrayList<>();
            entries.add(new PieEntry(50));
            entries.add(new PieEntry(50));
            entries.add(new PieEntry(50));
            addEnergyPieData(entries, false);



        myReceiver = new MyReceiver()  {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                String data = bundle.getString(DataLayerListenerService.DATA);
                try {
                    JSONObject json = new JSONObject(data);
                    if(json.has("value")) {
                        total = String.valueOf(json.getInt("value"));

                        hvac = String.valueOf(json.getInt("HVAC"));

                        light = String.valueOf(json.getInt("Light"));

                        electrical = String.valueOf(json.getInt("Electrical"));

                        ArrayList<PieEntry> entries = new ArrayList<>();
                        entries.add(new PieEntry(Float.valueOf(hvac)));
                        entries.add(new PieEntry(Float.valueOf(light)));
                        entries.add(new PieEntry(Float.valueOf(electrical)));
                        addEnergyPieData(entries, false);
                        energy.setText(SharedFunctions.getEnergyHTML(total, "Total Energy: "));
                    }
                    else if(json.has("location")) {
                        String loc = json.getString("location");
                        location.setText(loc);
                    }
                    else {
                        energy.setText("Unkown");
                        location.setText("Unkown");
                    }
                    Log.v("Main Activity", json.toString());
                }
                catch (JSONException e) {
                    Log.i("Error", "error on parsing ble reciever");
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DataLayerListenerService.DATA);
        registerReceiver(myReceiver, intentFilter);
    }
    @Override
    protected void onStop()
    {
        //unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent arg1) {
            // TODO Auto-generated method stub

            String data = arg1.getStringExtra(DataLayerListenerService.DATA);

            Intent i = new Intent().putExtra(DataLayerListenerService.DATA, data);
            context.sendBroadcast(i);
        }

    }

    public void changeEnergy(View v) {
        String message;
        String label;
        int c;
        if(count % 4 == 0) {
            message = total;
            label = "Total Energy: ";
            c = R.color.appWhite;
        }
        else if(count % 4 == 1) {
            message = hvac;
            label = "HVAC: ";
            c = R.color.HVAC;
        }
        else if(count % 4 == 2) {
            message = light;
            label = "Light: ";
            c = R.color.LIGHT;
        }
        else {
            message = electrical;
            label = "Electrical: ";
            c = R.color.ELECTRIC;
        }
        energy.setText(SharedFunctions.getEnergyHTML(message, label));
        energy.setTextColor(getResources().getColor(c));
        count++;
    }

    private void addEnergyPieData( ArrayList <PieEntry> entries, boolean ifZero) {
        PieDataSet pieDataSet = new PieDataSet(entries, "");
        pieDataSet.setDrawValues(false);
        if(!ifZero)
            pieDataSet.setColors(new int[] {R.color.HVAC, R.color.LIGHT, R.color.ELECTRIC}, getApplicationContext());
        else
            pieDataSet.setColors(new int[]{R.color.appGrey}, getApplicationContext());

        PieData pieData = new PieData(pieDataSet);
        energyPie.setData(pieData);
        energyPie.notifyDataSetChanged();
        energyPie.invalidate();
    }
}